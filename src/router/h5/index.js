import { createRouter, createWebHashHistory } from 'vue-router'
import '../../style/h5/index.css'
const router = createRouter({
  history: createWebHashHistory(), // hash模式：createWebHashHistory，history模式：createWebHistory
  routes: [
    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '/home',
      name: 'home',
      component: () => import(/* webpackChunkName: "home" */ '@/views/h5/Home.vue'),
      meta: {
        index: 1
      }
    },
    {
      path: '/daily',
      name: 'daily',
      component: () => import(/* webpackChunkName: "home" */ '@/views/h5/daily/daily.vue')
    }
  ]
})

export default router
