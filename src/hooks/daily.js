import {reactive, toRefs} from "vue";
export const useDaily=()=>{
    const data = reactive({
        active: 0
    })
    const day=new Date().getDay()
    data.active=day===0?6:day-1
    return {
        ...toRefs(data)
    };
}
